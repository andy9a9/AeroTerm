import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15

import Qt.labs.settings 1.0

Item {
    id: item1

    property alias cb_comPort: cb_comPort
    property alias bn_connectPort: bn_connectPort
    property alias bn_rescanPorts: bn_rescanPorts
    property alias bn_clearOutputText: bn_clearOutputText
    property alias bn_sendInputText: bn_sendInputText
    property alias gb_consoleInput: gb_consoleInput
    property alias sw_ConsoleStyle: sw_ConsoleStyle
    property alias consoleView: consoleView
    property alias inputTextArea: inputTextArea
    property alias bn_toggleLogging: bn_toggleLogging
    property alias bn_togglePlayback: bn_togglePlayback
    property alias bn_chooseLogFile: bn_chooseLogFile
    property alias bn_choosePlaybackFile: bn_choosePlaybackFile
    property alias bn_pausePlayback: bn_pausePlayback
    property alias playbackSlider: playbackSlider
    property alias userSearchTextInput: userSearchTextInput
    property alias customBaudRateText: customTextBaudRate.text
    property alias autoReconnectState: cb_autoReconnect.checked

    width: 800
    height: 600

    property int radioButtonFontPixel: 14

    Button {
        id: bn_connectPort
        x: 20
        y: 35
        width: 89
        height: 21
        text: qsTr("Connect")
    }

    CheckBox {
        id: cb_autoReconnect
        anchors.top: bn_connectPort.bottom
        anchors.left: bn_connectPort.left
        text: qsTr("Autoreconnect")
    }

    ConnectionFlag {
        id: connectionFlag

        anchors.right: gb_logging.left
        anchors.rightMargin: 20

        state: isConnected === false ? "fsm_disconnected" : "fsm_reconnected"
    }

    ScrollView {
        anchors.top: parent.top
        anchors.left: bn_connectPort.right
        anchors.right: gb_logging.left

        anchors.leftMargin: 20
        anchors.topMargin: 0
        height: contentHeight + 20

        ScrollBar.horizontal.policy: width >= contentWidth ? ScrollBar.AlwaysOff :  ScrollBar.AlwaysOn

        clip: true
        RowLayout {
            anchors.fill: parent
            anchors.topMargin: 10
            spacing: 5

            GroupBox {
                id: gb_comPorts

                Layout.minimumWidth: 200
                Layout.preferredWidth: 300
                Layout.maximumWidth: 400
                Layout.fillWidth: true

                Layout.preferredHeight: 50

                Layout.alignment: Qt.AlignTop

                padding: 8

                label: Rectangle {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.top
                    anchors.bottomMargin: -height/2
                    color: "white"                  //set this to the background color
                    width: comPortsLabel.implicitWidth + 10

                    height: comPortsLabel.font.pixelSize
                    Text {
                        id: comPortsLabel
                        text: qsTr("COM Port")
                        anchors.centerIn: parent
                        font.pixelSize: 10
                    }
                }


                Button {
                    id: bn_rescanPorts
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.right: parent.right

                    height: cb_comPort.height
                    width: height

                    icon.source: "qrc:/Resources/sync-alt-solid.svg"
                    icon.height: height - 2
                    icon.width: height

                }


                ComboBox {
                    id: cb_comPort
                    y: -1
                    height: 22

                    anchors.rightMargin: -2
                    anchors.leftMargin: 3
                    anchors.right: bn_rescanPorts.left
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom

                    textRole: "key"

                    font.pixelSize: 10
                    leftInset: 5
                    rightInset: 5
                }
            }

            GroupBox {
                id: gb_baudRates

                Layout.alignment: Qt.AlignTop
                Layout.preferredHeight: implicitContentHeight+bottomPadding+topPadding

                Layout.fillWidth: false
                Layout.preferredWidth: implicitContentWidth + leftPadding + rightPadding

                topPadding: 4
                bottomPadding: 4

                leftPadding: 2
                rightPadding: 2

                label: Rectangle {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.top
                    anchors.bottomMargin: -height/2
                    color: "white"                  //set this to the background color
                    width: baudRatesLabel.implicitWidth + 10

                    height: baudRatesLabel.font.pixelSize
                    Text {
                        id: baudRatesLabel
                        text: qsTr("Baud rate")
                        anchors.centerIn: parent
                        font.pixelSize: 10
                    }
                }


                GridLayout {
                    rows: 5
                    flow: GridLayout.TopToBottom

                    rowSpacing: 0
                    columnSpacing: 0

                    anchors.fill: parent
                    anchors.topMargin: 2

                    Repeater {
                        id: baudRateRepeater

                        model: [600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 57600, 115200, 250000, 500000, 1000000, -1]

                        property string customFieldLabel: qsTr("Custom")

                        RadioButton {
                            id: rb_baudRate

                            Layout.fillWidth: true
                            Layout.preferredHeight: indicator.height+2

                            property int baudRate: (modelData > 0) ? modelData : customBaudRateText*1
                            ButtonGroup.group: baudRateButtonGroup

                            text: (modelData > 0) ? baudRate : baudRateRepeater.customFieldLabel
                            font.pointSize: indicator.height

                            // First check if the button value is greater than 0. If so, then it is not the "Custom" button
                            checked: (modelData > 0) ? 
                                        (modelData === terminal.baudRate) :  // Check if the current baud rate matches the button value
                                        (baudRateRepeater.model.indexOf(terminal.baudRate) < 0)  // Check if the current baud rate is in the list of known baud rates. If it is not, then this means a custom baud rate is selected

                            padding: 0

                            Binding {
                                target: indicator
                                property: "width"
                                value: radioButtonFontPixel
                            }

                            Binding {
                                target: indicator
                                property: "height"
                                value: indicator.width
                            }

                            topInset: 0
                            bottomInset: 0

                            indicator: Rectangle {

                                implicitWidth: 28
                                implicitHeight: 28

                                x: rb_baudRate.text ? (rb_baudRate.mirrored ? rb_baudRate.width - width - rb_baudRate.rightPadding : rb_baudRate.leftPadding) : rb_baudRate.leftPadding + (rb_baudRate.availableWidth - width) / 2
                                y: rb_baudRate.topPadding + (rb_baudRate.availableHeight - height) / 2

                                radius: width / 2
                                color: rb_baudRate.down ? rb_baudRate.palette.light : rb_baudRate.palette.base
                                border.width: rb_baudRate.visualFocus ? 2 : 1
                                border.color: rb_baudRate.visualFocus ? rb_baudRate.palette.highlight : rb_baudRate.palette.mid

                                Rectangle {
                                    x: (parent.width - width) / 2
                                    y: (parent.height - height) / 2
                                    width: parent.width*20/28
                                    height: parent.height*20/28
                                    radius: width / 2
                                    color: rb_baudRate.palette.text
                                    visible: rb_baudRate.checked
                                }
                            }
                        }
                    }

                    Rectangle {
                        border.width: 1
                        color: "#100000ee"

                        Layout.leftMargin: 10
                        Layout.rightMargin: 0
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        TextInput {
                            id: customTextBaudRate
                            anchors.fill: parent
                            anchors.rightMargin: 1

                            validator: DoubleValidator{bottom: 1; top: 99e6;}
                            text: "1000000"
                            horizontalAlignment: TextInput.AlignRight
                            verticalAlignment: TextInput.AlignVCenter

                            font.family: "Courier"
                            font.pixelSize: radioButtonFontPixel

                            onTextChanged: {
                                // When the text is changed, start a timer to update the baud rate
                                customTextInputTimeout.start()
                            }
                        }

                        Timer {
                            id: customTextInputTimeout
                            running: false
                            repeat: false

                            interval: 500  //  500ms seems to be slow enough not to trigger between keystrokes, but fast enough still to feel instantaneous.

                            onTriggered: {
                                // Check if the selected button is the "Custom" one.
                                if (baudRateButtonGroup.checkedButton.text === baudRateRepeater.customFieldLabel) {
                                    terminal.baudRate = customBaudRateText*1
                                }
                            }
                        }
                    }
                }
            }

            GroupBox {
                id: gb_stopBits

                Layout.fillHeight: true

                Layout.fillWidth: false
                Layout.preferredWidth: (implicitContentWidth > stopBitsLabel.width ? implicitContentWidth : stopBitsLabel.width + 15) + leftPadding + rightPadding


                Layout.alignment: Qt.AlignTop

                topPadding: 4
                bottomPadding: 4

                leftPadding: 2
                rightPadding: 2


                label: Rectangle {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.top
                    anchors.bottomMargin: -height/2
                    color: "white"                  //set this to the background color
                    width: stopBitsLabel.implicitWidth + 10

                    height: stopBitsLabel.font.pixelSize
                    Text {
                        id: stopBitsLabel
                        text: qsTr("Stop bits")
                        anchors.centerIn: parent
                        font.pixelSize: 10
                    }
                }


                ColumnLayout {
                    anchors.fill: parent
                    anchors.topMargin: 2

                    Repeater {
                        model: [1, 1.5, 2]

                        RadioButton {
                            id: rb_stopBits

                            Layout.fillWidth: true
                            Layout.preferredHeight: indicator.height+2

                            property real stopBits: modelData
                            ButtonGroup.group: stopBitsButtonGroup

                            text: modelData
                            font.pointSize: indicator.height

                            checked: (modelData === 1)

                            padding: 0

                            Binding {
                                target: indicator
                                property: "width"
                                value: radioButtonFontPixel
                            }

                            Binding {
                                target: indicator
                                property: "height"
                                value: indicator.width
                            }

                            topInset: 0
                            bottomInset: 0

                            indicator: Rectangle {

                                implicitWidth: 28
                                implicitHeight: 28

                                x: rb_stopBits.text ? (rb_stopBits.mirrored ? rb_stopBits.width - width - rb_stopBits.rightPadding : rb_stopBits.leftPadding) : rb_stopBits.leftPadding + (rb_stopBits.availableWidth - width) / 2
                                y: rb_stopBits.topPadding + (rb_stopBits.availableHeight - height) / 2

                                radius: width / 2
                                color: rb_stopBits.down ? rb_stopBits.palette.light : rb_stopBits.palette.base
                                border.width: rb_stopBits.visualFocus ? 2 : 1
                                border.color: rb_stopBits.visualFocus ? rb_stopBits.palette.highlight : rb_stopBits.palette.mid

                                Rectangle {
                                    x: (parent.width - width) / 2
                                    y: (parent.height - height) / 2
                                    width: parent.width*20/28
                                    height: parent.height*20/28
                                    radius: width / 2
                                    color: rb_stopBits.palette.text
                                    visible: rb_stopBits.checked
                                }
                            }
                        }
                    }
                }
            }


            GroupBox {
                id: gb_parity

                Layout.fillHeight: true

                Layout.fillWidth: false
                Layout.preferredWidth: implicitContentWidth + leftPadding + rightPadding


                Layout.alignment: Qt.AlignTop

                topPadding: 4
                bottomPadding: 4

                leftPadding: 2
                rightPadding: 2

                label: Rectangle {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.top
                    anchors.bottomMargin: -height/2
                    color: "white"                  //set this to the background color
                    width: parityLabel.implicitWidth + 2

                    height: parityLabel.font.pixelSize
                    Text {
                        id: parityLabel
                        text: qsTr("Parity bits")
                        anchors.centerIn: parent
                        font.pixelSize: 10
                    }
                }

                ColumnLayout {
                    anchors.fill: parent
                    anchors.topMargin: 2

                    spacing: 0

                    Repeater {
                        model: ["none", "even", "odd", "space", "mark"]

                        RadioButton {
                            id: rb_parity

                            Layout.fillWidth: true
                            Layout.preferredHeight: indicator.height+2

                            property string parity: modelData
                            ButtonGroup.group: parityButtonGroup

                            text: modelData
                            font.pointSize: indicator.height

                            checked: (modelData === "none")

                            padding: 0

                            Binding {
                                target: indicator
                                property: "width"
                                value: radioButtonFontPixel
                            }

                            Binding {
                                target: indicator
                                property: "height"
                                value: indicator.width
                            }

                            topInset: 0
                            bottomInset: 0

                            indicator: Rectangle {

                                implicitWidth: 28
                                implicitHeight: 28

                                x: rb_parity.text ? (rb_parity.mirrored ? rb_parity.width - width - rb_parity.rightPadding : rb_parity.leftPadding) : rb_parity.leftPadding + (rb_parity.availableWidth - width) / 2
                                y: rb_parity.topPadding + (rb_parity.availableHeight - height) / 2

                                radius: width / 2
                                color: rb_parity.down ? rb_parity.palette.light : rb_parity.palette.base
                                border.width: rb_parity.visualFocus ? 2 : 1
                                border.color: rb_parity.visualFocus ? rb_parity.palette.highlight : rb_parity.palette.mid

                                Rectangle {
                                    x: (parent.width - width) / 2
                                    y: (parent.height - height) / 2
                                    width: parent.width*20/28
                                    height: parent.height*20/28
                                    radius: width / 2
                                    color: rb_parity.palette.text
                                    visible: rb_parity.checked
                                }
                            }
                        }
                    }
                }
            }


            GroupBox {
                id: gb_dataBits

                Layout.fillHeight: true

                Layout.fillWidth: false
                Layout.preferredWidth: (implicitContentWidth > dataBitsLabel.width ? implicitContentWidth : dataBitsLabel.width + 15) + leftPadding + rightPadding

                Layout.alignment: Qt.AlignTop

                topPadding: 4
                bottomPadding: 4

                leftPadding: 2
                rightPadding: 2

                label: Rectangle {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.top
                    anchors.bottomMargin: -height/2
                    color: "white"                  //set this to the background color
                    width: dataBitsLabel.implicitWidth + 2

                    height: dataBitsLabel.font.pixelSize
                    Text {
                        id: dataBitsLabel
                        text: qsTr("Data bits")
                        anchors.centerIn: parent
                        font.pixelSize: 10
                    }
                }


                ColumnLayout {
                    anchors.fill: parent
                    anchors.topMargin: 2

                    Repeater {
                        model: [5, 6, 7, 8]

                        RadioButton {
                            id: rb_dataBits

                            Layout.fillWidth: true
                            Layout.preferredHeight: indicator.height+2

                            property real dataBits: modelData
                            ButtonGroup.group: dataBitsButtonGroup

                            text: modelData
                            font.pointSize: indicator.height

                            checked: (modelData === 8)

                            padding: 0

                            Binding {
                                target: indicator
                                property: "width"
                                value: radioButtonFontPixel
                            }

                            Binding {
                                target: indicator
                                property: "height"
                                value: indicator.width
                            }

                            topInset: 0
                            bottomInset: 0

                            indicator: Rectangle {

                                implicitWidth: 28
                                implicitHeight: 28

                                x: rb_dataBits.text ? (rb_dataBits.mirrored ? rb_dataBits.width - width - rb_dataBits.rightPadding : rb_dataBits.leftPadding) : rb_dataBits.leftPadding + (rb_dataBits.availableWidth - width) / 2
                                y: rb_dataBits.topPadding + (rb_dataBits.availableHeight - height) / 2

                                radius: width / 2
                                color: rb_dataBits.down ? rb_dataBits.palette.light : rb_dataBits.palette.base
                                border.width: rb_dataBits.visualFocus ? 2 : 1
                                border.color: rb_dataBits.visualFocus ? rb_dataBits.palette.highlight : rb_dataBits.palette.mid

                                Rectangle {
                                    x: (parent.width - width) / 2
                                    y: (parent.height - height) / 2
                                    width: parent.width*20/28
                                    height: parent.height*20/28
                                    radius: width / 2
                                    color: rb_dataBits.palette.text
                                    visible: rb_dataBits.checked
                                }
                            }
                        }
                    }
                }
            }

        }
    }


    ConsoleView {
        id: consoleView

        y: 163
        height: gb_consoleInput.visible ? Window.height - 30 - y - gb_consoleInput.height: Window.height - 30 - y
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 20
        anchors.rightMargin: anchors.leftMargin

        isConsoleOnlyView: !sw_displayHex.checked
    }

    Text {
        id: maxCharacters_txt
        anchors.top: consoleView.bottom
        anchors.right: consoleView.right

        color: "grey"

        text: consoleView.count + " / " + consoleView.maxCharacters
    }


    Label {
        text: "Console output"
        x: 30

        anchors.bottom: consoleView.top
        anchors.bottomMargin: 5
    }


    GroupBox {
        id: gb_consoleInput

        height: 93
        anchors.top: consoleView.bottom
        anchors.topMargin: 6
        anchors.left: consoleView.left
        anchors.right: gb_playback.left
        anchors.rightMargin: 10
        bottomPadding: 0
        rightPadding: 0
        leftPadding: 0
        title: qsTr("Console input")


        Flickable {
            anchors.fill: parent

            clip: true

            TextArea.flickable: TextArea {
                id: inputTextArea
                font.pointSize: 10
                font.family: "Courier"

                selectByKeyboard: true
                selectByMouse: true

                placeholderText: "Enter text here. Supports hex escapes, e.g. `\\xff\\x00` transmits `0xFF00`.\n\nSend either by pressing the Send button or <SHIFT>-<ENTER>."
                placeholderTextColor: "#777777"

                wrapMode: TextArea.Wrap

                Keys.onReturnPressed: { _onEnterPressed(event) }
                Keys.onEnterPressed: { _onEnterPressed(event) }

                // Captue <SHIFT>-<ENTER>
                function _onEnterPressed(event)
                {
                    if ((event.modifiers & Qt.ShiftModifier)) {
                        bn_sendInputText.clicked()
                    } else {
                        event.accepted = false;
                    }
                }
            }

            ScrollBar.vertical: ScrollBar { }
        }
    }

    Button {
        id: bn_clearOutputText
        x: 136
        z: consoleView.z + 1
        width: 96
        height: 17
        text: qsTr("Clear")
        anchors.bottom: consoleView.top
        anchors.bottomMargin: 5
    }

    Button {
        id: bn_sendInputText
        z: gb_consoleInput.z + 1
        height: 17
        text: qsTr("Send")
        anchors.top: gb_consoleInput.top
        anchors.topMargin: 0
        anchors.right: bn_clearOutputText.right
        anchors.rightMargin: 0
        anchors.left: bn_clearOutputText.left
        anchors.leftMargin: 0
    }

    Switch {
        id: sw_ConsoleStyle
        x: 230
        y: 75
        text: qsTr("Terminal style")
    }

    Text {
        id: lb_sensorStyle
        text: qsTr("Sensor style")
        anchors.verticalCenterOffset: 0
        anchors.right: sw_ConsoleStyle.left
        anchors.rightMargin: 0
        anchors.verticalCenter: sw_ConsoleStyle.verticalCenter
        font.pixelSize: 12
    }

    Switch {
        id: sw_displayHex

        anchors.right: userSearchText_bx.left
        anchors.verticalCenter: userSearchText_bx.verticalCenter

        anchors.rightMargin: 20

        text: qsTr("Hex")

    }

    Settings {
        category: "TerminalViewSettings"
        property alias sw_displayHex_checked: sw_displayHex.checked
        // ...
    }

    Rectangle {
        id: userSearchText_bx
        anchors.right: gb_logging.right
        anchors.bottom: consoleView.top

        anchors.bottomMargin: 10


        height: 25
        width: 200
        radius: 4

        border.width: 1
        color: "#100000ee"

        TextInput {
            id: userSearchTextInput
            anchors.fill: parent
            anchors.rightMargin: 9

            horizontalAlignment: TextInput.AlignRight
            verticalAlignment: TextInput.AlignVCenter

            font.family: "Courier"
            font.pixelSize: 12

            RoundButton {
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: -8

                scale: .5

                icon.source: "qrc:/Resources/search-solid.svg"
            }

            Text {
                anchors.rightMargin: 10
                anchors.right: parent.right
                anchors.verticalCenter: userSearchTextInput.verticalCenter

                color: "#aaa"

                visible: !parent.text

                text: "Search for text"
            }
        }

    }


    GroupBox {
        id: gb_logging

        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10

        padding: 8

        label: Rectangle {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.top
            anchors.bottomMargin: -height/2
            color: "white"                  //set this to the background color
            width: loggingLabel.implicitWidth + 10

            height: loggingLabel.font.pixelSize
            Text {
                id: loggingLabel
                text: qsTr("Logging")
                anchors.centerIn: parent
                font.pixelSize: 10
            }
        }

        ColumnLayout {
            anchors.fill: parent

            Button {
                id: bn_chooseLogFile
                text: qsTr("Choose file")
                font.pixelSize: 11
                Layout.preferredHeight: 35
                Layout.preferredWidth: 75
            }

            Button {
                id: bn_toggleLogging
                text: qsTr("Start")
                font.pixelSize: 11
                Layout.fillWidth: true
                Layout.preferredHeight: 35
                Layout.preferredWidth: 75
            }
        }
    }


    GroupBox {
        id: gb_playback

        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottomMargin: 10

        padding: 8

        label: Rectangle {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.top
            anchors.bottomMargin: -height/2
            color: "white"                  //set this to the background color
            width: playbackLabel.implicitWidth + 10

            height: playbackLabel.font.pixelSize
            Text {
                id: playbackLabel
                text: qsTr("Playback")
                anchors.centerIn: parent
                font.pixelSize: 10
            }
        }

        GridLayout {
            flow: GridLayout.LeftToRight
            columns: 3

            anchors.fill: parent

            Button {
                id: bn_choosePlaybackFile
                text: qsTr("Choose file")

                font.pixelSize: 11
                Layout.preferredHeight: 35
                Layout.preferredWidth: 75
            }

            Button {
                id: bn_togglePlayback
                text: qsTr("Start")

                font.pixelSize: 11
                Layout.preferredHeight: 35
                Layout.preferredWidth: 75
            }

            Button {
                id: bn_pausePlayback
                text: qsTr("Pause")

                enabled: false

                font.pixelSize: 11
                Layout.preferredHeight: 35
                Layout.preferredWidth: 75
            }

            Slider {
                id: playbackSlider

                from: -2
                to: 2

                value: 0

                Layout.fillWidth: true
                Layout.columnSpan: 3

                property real logValue: Math.pow(10, value)

                onLogValueChanged: {
                    playbackTimer.timeCompression = playbackSlider.logValue
                }

                handle: Rectangle {
                    id: sliderHandle

                    x: playbackSlider.leftPadding + playbackSlider.visualPosition * (playbackSlider.availableWidth - width)
                    y: playbackSlider.topPadding + playbackSlider.availableHeight / 2 - height / 2
                    implicitWidth: 30
                    implicitHeight: 20
                    radius: 3
                    color: playbackSlider.pressed ? "#f0f0f0" : "#f6f6f6"
                    border.color: "#bdbebf"

                    Text {
                        text: playbackSlider.logValue.toPrecision(2) + "x"
                    }
                }
            }
        }
    }
}
