import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.platform 1.1

SerialTerminalForm {
    property alias comPortListModel: comPortListModel
    property alias baudRateButtonGroup: baudRateButtonGroup
    property alias dataBitsButtonGroup: dataBitsButtonGroup
    property alias stopBitsButtonGroup: stopBitsButtonGroup
    property alias parityButtonGroup: parityButtonGroup
    property string selectedPort
    property bool isConnected: false
    property int baudRate: 57600
    property string parity: "none"
    property real stopBits: 1
    property int dataBits: 8

    ListModel {
        id: comPortListModel
    }

    bn_connectPort.text: isConnected ? qsTr("Disconnect") : qsTr("Connect")
    bn_rescanPorts.enabled: !isConnected
    bn_sendInputText.enabled: isConnected
    bn_sendInputText.visible: !sw_ConsoleStyle.checked
    gb_consoleInput.visible: !sw_ConsoleStyle.checked

    cb_comPort.onCurrentIndexChanged: {
        if (comPortListModel.count > 0) {
            selectedPort = comPortListModel.get(cb_comPort.currentIndex).value
            console.log("Chosen: " + selectedPort)
        }
    }

    cb_comPort.model: comPortListModel

    bn_clearOutputText.onClicked: {
        consoleView.clear()
    }

    bn_sendInputText.onClicked: {
        // Convert the text into a byte array
        let [outputArray, idx] = hexadecimalEscapeToArr(inputTextArea.text)

        // Send bytes
        let ret = serial.writeBytes(outputArray.buffer);

        // Check that all is well
        if (ret === idx) {
            consoleView.addSentData(outputArray.buffer)

            if (fileIO.isLoggingActive) {
                // Write the data format in JSON as a hex-escaped string with an associated timestamp.
                let output = {}
                output.timestamp = Date.now()
                output.tx_data = inputTextArea.text

                fileIO.write(JSON.stringify(output) + "\n")
            }

        } else {
            if (ret !== -1) {
                console.log("[WARNING] Failed to send: " + outputArray + ", only sent " + ret + " bytes.")
            } else {
                console.log("[WARNING] Failed to send: " + outputArray)
            }
        }


    }

    ButtonGroup {
        id: baudRateButtonGroup
        onCheckedButtonChanged: {
            // Change the baud rate value
            baudRate = checkedButton.baudRate;
        }
    }

    ButtonGroup {
        id: dataBitsButtonGroup
        onCheckedButtonChanged: {
            // Change the baud rate value
            dataBits = checkedButton.dataBits;
        }
    }

    ButtonGroup {
        id: stopBitsButtonGroup
        onCheckedButtonChanged: {
            // Change the baud rate value
            stopBits = checkedButton.stopBits;
        }
    }

    ButtonGroup {
        id: parityButtonGroup
        onCheckedButtonChanged: {
            // Change the baud rate value
            parity = checkedButton.parity;
        }
    }

    bn_toggleLogging.onClicked: {
        if (fileIO.isLoggingActive) {
            bn_toggleLogging.text = "Start"
            fileIO.isLoggingActive = false
            fileIO.close()
        } else if (fileIO.isPathValid){
            bn_toggleLogging.text = "Stop"
            fileIO.openWriteOnly()
            fileIO.isLoggingActive = true
        } else {
            console.log("Didn't toggle the button. Something went wrong.")
        }
    }

    bn_togglePlayback.onClicked: {
        if (fileIO.isPlaybackActive === true) {
            stopPlayback();
        } else if (fileIO.isPathValid === true){
            startPlayback()
        } else {
            console.log("[ERROR] Didn't toggle the button. Something went wrong.")
        }
    }


    Connections {
        target: mainWindow
        function onPlaybackReachedEndOfLogFile() {
            console.log("[SerialTerminal] End of log file");

            stopPlayback();
        }
    }


    FileDialog {
        id: fileDialog
        defaultSuffix: '.log'
        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)

        onFileChanged: {
            fileIO.filename = file
        }

        onAccepted: {
            fileIO.isPathValid = true;
        }
    }

    bn_chooseLogFile.onClicked: {
        fileDialog.currentFile = new Date().toLocaleString(Qt.locale(), "yyyyMMdd-HH'h'mm'm'ss's'")
        fileDialog.fileMode = FileDialog.SaveFile
        fileDialog.open()
    }

    bn_choosePlaybackFile.onClicked: {
        fileDialog.fileMode = FileDialog.OpenFile
        fileDialog.open()
    }

    bn_pausePlayback.onClicked: {
        if (playbackTimer.running) {
            playbackTimer.stop()
            bn_pausePlayback.text = "Unpause"
        } else {
            playbackTimer.start()
            bn_pausePlayback.text = "Pause"
        }
    }

    userSearchTextInput.onTextChanged: {
        terminal.consoleView.userSearchText = userSearchTextInput.text
    }

    function startPlayback() {
        fileIO.openReadOnly()
        fileIO.isPlaybackActive = true

        playbackTimer.wallTimeOffset = Date.now()
        playbackTimer.start()

        bn_togglePlayback.text = "Stop"

        bn_pausePlayback.enabled = true
    }


    function stopPlayback() {

        playbackTimer.stop()
        playbackTimer.timeStampOffset = undefined
        playbackTimer.isNextMessageEmpty = true

        fileIO.close()
        fileIO.isPlaybackActive = false

        bn_togglePlayback.text = "Start"

        bn_pausePlayback.enabled = false
    }
}
