import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0
import QtQuick.Shapes 1.0

import SyntaxHighlighter 1.0

//! Everything inside this goes into its own component file
Item {
//    border.width: 1

    // Make these properties "private"
    // C.f. https://stackoverflow.com/questions/13252158/does-qml-support-access-specifiers-like-private-for-properties
    QtObject {
        id: internal
        property var canonicalArray: new Uint8Array()
    }

    property bool isConsoleOnlyView: false

    property int maxCharacters: 20000

    property int count: outputTextArea.text.length

    property string consoleFont: "Courier"
    property int consolePixelSize: 12

    property int hexValuesPerRow: consoleCharactersPerRow*3


    //! VVVVVVVVVVVVVVVVVVVVVVVVV
    //  The belows section deals with syntax highlighting
    property string userSearchText
    property var userSearchRegex: /(?:)/g  // Default regex, doesn't do anything

    property string highlightedHexSearch

    onUserSearchTextChanged: {
        highlightedHexSearch = asciiToHex(userSearchText)

        // Convert regex-overridden characters into escape characters
        // C.f. https://stackoverflow.com/a/35478115
        let tmpStr = userSearchText.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&')

        userSearchRegex = new RegExp("(" + tmpStr + ")", "g");

        // Force the views to update
        // Silly way to do it, but it's a workaround for https://bugreports.qt.io/browse/QTBUG-94704
        // C.f. https://bugreports.qt.io/browse/QTBUG-65248?focusedCommentId=441726&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-441726
        outputTextHighlighter.refresh()
        hexHighlighter.refresh()
    }

    TextCharFormat { id: highlightedFormat; background: "lightblue" }

    SyntaxHighlighter {

        id: outputTextHighlighter
        textDocument: outputTextArea.textDocument
        onHighlightBlock: {
            let m
            let regex = userSearchRegex
            let str = text

            // Check that the regular expression's global flag is set
            if (regex.global !== true) {
                console.assert(false, "Regex global flag is not set: " + userSearchRegex)
                return;
            }

            // CAUTION: regex global flag *must* be set or else this will loop infinitely
            while ((m = regex.exec(str)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }

                // Set this highlighter's formatting
                setFormat(m.index, m[0].length, highlightedFormat);
            }
        }
    }

    SyntaxHighlighter {
        id: hexHighlighter
        textDocument: outputHexArea.textDocument

        onHighlightBlock: {
            let hexRegex = new RegExp(highlightedHexSearch, "g");

            let m
            let regex = hexRegex
            let str = text

            // Check that the regular expression's global flag is set
            if (regex.global !== true) {
                console.assert(false, "Regex global flag is not set: " + userSearchRegex)
                return;
            }

            // CAUTION: regex global flag *must* be set or else this will loop infinitely
            while ((m = regex.exec(str)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }

                setFormat(m.index, m[0].length, highlightedFormat);
            }
        }

    }


    // Calculate the number of characters we can fit in a single row
    /*
        width = (consoleCharactersPerRow + hexValuesPerRow)*fontObject.fontPixelWidth + padding + scrollBar.width + extra + consoleCharactersPerRow*font.wordSpacing
        width = (consoleCharactersPerRow + hexValuesPerRow)*fontObject.fontPixelWidth + padding + scrollBar.width + extra + consoleCharactersPerRow*font.wordSpacing
        width = (consoleCharactersPerRow + 3*consoleCharactersPerRow)*fontObject.fontPixelWidth + padding + scrollBar.width + extra + consoleCharactersPerRow*font.wordSpacing
        width = (4*consoleCharactersPerRow)*fontObject.fontPixelWidth + (padding + scrollBar.width + extra)+ consoleCharactersPerRow*font.wordSpacing
        width - (padding + scrollBar.width + extra) = (4*consoleCharactersPerRow)*fontObject.fontPixelWidth + consoleCharactersPerRow*font.wordSpacing
        (width - (padding + scrollBar.width + extra)) = 4*consoleCharactersPerRow*fontObject.fontPixelWidth + consoleCharactersPerRow*font.wordSpacing
        (width - (padding + scrollBar.width + extra)) = (4*fontObject.fontPixelWidth + font.wordSpacing) * consoleCharactersPerRow
        (width - (padding + scrollBar.width + extra))/(4*fontObject.fontPixelWidth + font.wordSpacing) = consoleCharactersPerRow
    */
    property int consoleCharactersPerRow: Math.floor((outputBox.width - (outputBox.padding + consoleScrollBar.width + 6))/(4*fontObject.fontPixelWidth + outputHexArea.font.wordSpacing))

    onConsoleCharactersPerRowChanged: {
        generateHexRowAddresses()
    }


    onIsConsoleOnlyViewChanged: {
        // Save selected console text
        let consoleSelectionStart
        let consoleSelectionEnd

        consoleSelectionStart = outputTextArea.selectionStart
        consoleSelectionEnd = outputTextArea.selectionEnd

        // Update the array

        let tmpArray = internal.canonicalArray
        internal.canonicalArray = new Uint8Array()
        updateText(tmpArray.buffer, true)

        let newStart
        let newEnd

        // Branch based on the new state
        if (isConsoleOnlyView) {
            // Convert the non-printing selection into a printing selection
            newStart = nonPrintingToPrinting(internal.canonicalArray, consoleSelectionStart)
            newEnd = nonPrintingToPrinting(internal.canonicalArray, consoleSelectionEnd)
        } else {
            // Convert the printing selection into a non-printing selection
            newStart = printingToNonPrinting(internal.canonicalArray, consoleSelectionStart)
            newEnd = printingToNonPrinting(internal.canonicalArray, consoleSelectionEnd)
        }

        // Reapply selected text
        outputTextArea.select(newStart, newEnd)
        outputTextArea.syncHexAreaSelection(outputTextArea.selectionStart, outputTextArea.selectionEnd)
    }


    Item {
        id: fontObject
        visible: false

        //! This text box lets us define what the pixel width is of a single character
        Text {
            id: calibratedText
            text: "1234567890"

            font.pixelSize: consolePixelSize
            font.family: consoleFont
        }

        property real fontPixelWidth: calibratedText.paintedWidth/calibratedText.text.length
        property real fontPixelHeight: calibratedText.paintedHeight

    }


    TableView {
        id: tableView
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: consoleScrollBar.left

        visible: !isConsoleOnlyView

        rowSpacing: 0

        clip: true

        model: TableModel {
            id: tableModel
            TableModelColumn { display: "address" }

            rows: [
                {
                    "address": "0000",
                }
            ]
        }

        //! Link the flickables together so that one bar moves both. Turn off reception of mouse events so that this cannot scroll on its own
        contentY: console_Flickable.contentY
        enabled: false
/*
  We should ideally beusing the below code instead of the jenky `contentY` and `enabled` approach,
  however, when we use the below commented code the system bugs when adding new text. In specific,
  the TextArea doesn't scroll to the bottom, but instead goes somewhere inbetween. I think what is
  happening is that the various scroll settings are overwriting each other in an asynchronous
  manner.
        //! Link the scrollbars together so that one bar moves both
        ScrollBar.vertical: consoleScrollBar
*/

        delegate: Rectangle {
            implicitWidth: tableView.width
            implicitHeight: fontObject.fontPixelHeight
            color: (index % 2) === 0 ? "#F0F0F0" : "white"

            Text {
                text: display

                font.pixelSize: consolePixelSize
                font.family: consoleFont
            }
        }

        property real columnWidth

        onRowsChanged: {
            if (rows > 0) {
                columnWidth = tableModel.getRow(rows-1).address.length * fontObject.fontPixelWidth
            } else {
                columnWidth = fontObject.fontPixelWidth
            }
        }

    }

    Item {
        id: outputBox

        anchors.fill: parent
        anchors.leftMargin: isConsoleOnlyView ? 0 : tableView.columnWidth + 15

        property int padding: 20

        Item {
            id: hexOutput_box

            visible: width > 0

            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left

            width: !isConsoleOnlyView ? (hexValuesPerRow * fontObject.fontPixelWidth + consoleCharactersPerRow*outputHexArea.font.wordSpacing) : 0

            Flickable {
                anchors.fill: parent

                boundsBehavior: Flickable.DragOverBounds
                flickableDirection: Flickable.VerticalFlick

                //! Link the scrollbars together so that one bar moves both
                ScrollBar.vertical: consoleScrollBar

                clip: true

                TextArea.flickable: TextArea {
                    id: outputHexArea

                    padding: 0

                    font.pixelSize: consolePixelSize
                    font.family: consoleFont
                    font.wordSpacing: -2

                    onFocusChanged: {
                        if(focus) {
                            outputTextArea.focus = false
                        }
                    }

                    placeholderText: "DE AD BE EF"

                    selectByKeyboard: true
                    selectByMouse: true
                    persistentSelection: true
                    readOnly: true
                    wrapMode: TextArea.Wrap

                    textFormat: TextEdit.PlainText

                    selectedTextColor: "black"
                    selectionColor: focus ? "#B4D5FE" : "lightgrey"

                    onSelectedTextChanged: {
                        if (focus) {
                            syncConsoleAreaSelection(selectionStart, selectionEnd)
                        }
                    }

                    function syncConsoleAreaSelection(start, end) {
                        let consoleSelectionStart = Math.ceil(selectionStart/3)
                        let consoleSelectionEnd = Math.ceil(selectionEnd/3)

                        // Saturate `selectionEnd`
                        if (consoleSelectionEnd > outputHexArea.text.length) {
                            consoleSelectionEnd = outputHexArea.text.length;
                        }

                        outputTextArea.select(consoleSelectionStart, consoleSelectionEnd)
                    }

                }

                Repeater {
                    property int numVerticalLines: Math.ceil(consoleCharactersPerRow/4)-1

                    model: numVerticalLines >= 0 ? numVerticalLines : 0

                    delegate: Shape {
                        property real columnWidth: 4*((fontObject.fontPixelWidth)*3 + outputHexArea.font.wordSpacing)
                        property real columnOffset: fontObject.fontPixelWidth/2 - 2

                        ShapePath {

                            strokeColor: "#999999"
                            strokeWidth: 1
                            strokeStyle: ShapePath.SolidLine
                            startX: columnOffset + (index+1)*columnWidth
                            startY: 0
                            PathLine {
                                x: columnOffset + (index+1)*columnWidth;
                                y: outputHexArea.height
                            }
                        }
                    }
                }
            }

            //! This MouseArea serves to replace the TextEdit selection tool. This is because
            //  the mouse interaction with a TextEdit does not return the cursor drag location,
            //  so we had to do it ourselves.
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                property int positionPressed
                z: 10

                onPositionChanged: {
                    let tmp = outputHexArea.positionAt(mouseX, mouseY)
                    let currentPosition
                    if (positionPressed < tmp) {
                        currentPosition = tmp + 3 - (tmp %3)

                        // Saturate `currentPosition`
                        if (currentPosition > outputHexArea.text.length) {
                            currentPosition = outputHexArea.text.length;
                        }
                    } else {
                        currentPosition = tmp - (tmp %3)

                        // Saturate `currentPosition`
                        if (currentPosition < 0) {
                            currentPosition = 0;
                        }
                    }

                    outputHexArea.select(positionPressed, currentPosition)
                }

                onPressed: {
                    let tmp = outputHexArea.positionAt(mouseX, mouseY)
                    if (tmp < outputHexArea.text.length) {
                        positionPressed = tmp - (tmp %3)
                    } else {
                        positionPressed = tmp
                    }

                    outputHexArea.focus = true
                }

                onReleased: {
                    if (positionPressed === outputHexArea.text.length &&
                            outputHexArea.positionAt(mouseX, mouseY) === outputHexArea.text.length) {
                        outputHexArea.deselect()
                    }
                }

            }
        }

        Item {
            id: consoleOutput_box

            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: isConsoleOnlyView ? parent.left : hexOutput_box.right

            width: isConsoleOnlyView ? parent.width : consoleCharactersPerRow * fontObject.fontPixelWidth + consoleScrollBar.width/* + 6*/

            anchors.leftMargin: isConsoleOnlyView ?  0 : outputBox.padding

            Flickable {
                id: console_Flickable
                anchors.fill: parent

                boundsBehavior: Flickable.DragOverBounds
                flickableDirection: Flickable.VerticalFlick

                clip: true

                TextArea.flickable: TextArea {
                    id: outputTextArea

                    topPadding: 0
                    bottomPadding: 0
                    leftPadding: 0
                    rightPadding: 3

                    font.pixelSize: consolePixelSize
                    font.family: consoleFont

                    placeholderText: isConsoleOnlyView ? " \





                               _______
              /\\              |__   __|
             /  \\   ___ _ __ ___ | | ___ _ __ _ __ ___
            / /\\ \\ / _ \\ '__/ _ \\| |/ _ \\ '__| '_ ` _ \\
           / ____ \\  __/ | | (_) | |  __/ |  | | | | | |
          /_/    \\_\\___|_|  \\___/|_|\\___|_|  |_| |_| |_|

    " : ""

                    selectByKeyboard: true
                    selectByMouse: true
                    persistentSelection: true
                    readOnly: true
                    wrapMode: isConsoleOnlyView ? TextArea.Wrap : TextArea.WrapAnywhere

                    textFormat: TextEdit.PlainText

                    selectionColor: (focus || isConsoleOnlyView) ? "#B4D5FE" : "lightgrey"

                    onSelectedTextChanged: {
                        if (focus) {
                            syncHexAreaSelection(selectionStart, selectionEnd)
                        }

                    }

                    onFocusChanged: {
                        if(focus) {
                            outputHexArea.focus = false
                        }
                    }

                    function syncHexAreaSelection(start, end) {
                        let hexSelectionStart = selectionStart*3
                        let hexSelectionEnd = selectionEnd*3

                        // Saturate `selectionEnd`
                        if (hexSelectionEnd > outputHexArea.text.length) {
                            hexSelectionEnd = outputHexArea.text.length;
                        }

                        outputHexArea.select(hexSelectionStart, hexSelectionEnd)

                    }
                }

                Keys.forwardTo: [consoleOutputKeyHandler]

                ScrollBar.vertical: consoleScrollBar

                Repeater {
                    model: sentData

                    delegate: Rectangle {
                        color: "transparent"
                        width: 10
                        height: width

                        property var positionRect: outputTextArea.positionToRectangle(position)

                        x: positionRect.x - fontObject.fontPixelWidth/2
                        y: positionRect.y - fontObject.fontPixelHeight/4 + 1

                        MouseArea {
                            anchors.fill: parent
                            anchors.margins: -5
                            preventStealing: true

                            onClicked: {
                                console.log("sentData: " + ("" + sentData) + ", " + "position: " + position + ", " + "timestamp: " + timestamp)
                            }
                        }

                        Canvas {
                            id: root
                            // canvas size
                            anchors.fill: parent
                            // handler to override for drawing
                            onPaint: {
                                // get context to draw with
                                var ctx = getContext("2d")
                                // setup the stroke
                                ctx.lineWidth = 1
                                ctx.strokeStyle = "blue"
                                // setup the fill
                                ctx.fillStyle = "steelblue"
                                // begin a new path to draw
                                ctx.beginPath()
                                // top-left start point
                                ctx.moveTo(0,0)
                                // upper line
                                ctx.lineTo(5,0)
                                // downward line
                                ctx.lineTo(2.5,2.5)
                                // left line through path closing
                                ctx.closePath()
                                // fill using fill style
                                ctx.fill()
                                // stroke using line width and stroke style
                                ctx.stroke()
                            }
                        }
                    }
                }
            }
        }
    }

    ScrollBar {
        id: consoleScrollBar
        policy: ScrollBar.AlwaysOn

        orientation: Qt.Vertical
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }


    function generateHexRowAddresses() {
        // Don't use `outputTextArea.lineCount`, there seems to be a race condition somewhere with that property
        let numRows = Math.ceil(outputTextArea.text.length / consoleCharactersPerRow);
        tableModel.clear()

        for (let i=0; i<numRows; i++) {

            let paddedString = "00000000000"+ (consoleCharactersPerRow * i).toString(16)

            let numDigits = 0

            let tmp = outputTextArea.text.length

            do {
                numDigits = numDigits + 1
                tmp = tmp / 16

            } while (tmp >= 1)


            let d = paddedString.substr(paddedString.length-numDigits);
            tableModel.appendRow({"address": d})
        }
    }

    function arrayToHex(array) {
        let hexString = ""
        for (let i=0; i<array.length; i++) {
            let c = array[i];
            let d = "00"+c.toString(16);
            let e = d.substr(d.length-2);
            hexString = hexString + " " + e
        }

        hexString = hexString.substring(1, hexString.length)

        return hexString

    }

    function asciiToHex(ascii) {
        let hexString = ""
        for (let i=0; i<ascii.length; i++) {
            let c = ascii.charCodeAt(i);
            let d = "00"+c.toString(16);
            let e = d.substr(d.length-2);
            hexString = hexString + " " + e
        }

        hexString = hexString.substring(1, hexString.length)

        return hexString
    }


    ListModel {
        id: sentData
        onCountChanged: {
            console.log("count: " + count)
        }
    }

    function addSentData(data) {
        sentData.append(
                    {
                        "sentData": Array.from(new Uint8Array(data)),
                        "timestamp": Date.now(),
                        "position": count
                    })
    }


    function clear() {
        internal.canonicalArray = new Uint8Array()
        updateText(internal.canonicalArray.buffer, true);

        sentData.clear();
    }


    function nonPrintingToPrinting(arrayBuffer_view, pos) {
        let characterCount = 0;
        for (let i=0; i<pos; i++) {
            // Only increment the character count if the character is printable OR if it is a Unix newline
            if ((arrayBuffer_view[i] >= 0x20 && arrayBuffer_view[i] <= 0x7e) || arrayBuffer_view[i] === 0x0a) {
                characterCount++;
            }
        }

        return characterCount;
    }


    function printingToNonPrinting(arrayBuffer_view, pos) {
        let characterCount = 0
        for (let i=0; i<arrayBuffer_view.length; i++) {

            if (characterCount === pos) {
                return i;
            }

            if ((arrayBuffer_view[i] >= 0x20 && arrayBuffer_view[i] <= 0x7e) || arrayBuffer_view[i] === 0x0a) {
                characterCount++;
            }

        }
    }


    function updateText(nonPrintingBuffer, mustRedrawAll) {
        // Check that the argument is an `ArrayBuffer`
        if (nonPrintingBuffer.__proto__.constructor.name !== "ArrayBuffer") {
            console.assert(false, "[ConsoleView] nonPrintingBuffer is of type " + nonPrintingBuffer.__proto__.constructor.name + ", but it must be an ArrayBuffer" )
            return;
        }

        let nonPrintingBuffer_view = new Uint8Array(nonPrintingBuffer); // treat buffer as a sequence of 32-bit integers
        let d = concatTypedArrays(internal.canonicalArray, nonPrintingBuffer_view);
        internal.canonicalArray = d

        let value

        // Get the current position and end position. Do this before adding the new text.
        let endPos = getEndPos();
        let pos = currPos();


        if (isConsoleOnlyView === false) {
            // Loop over the entire data set and append printing and non-printing characters
            let printingString = ""
            for(let num of nonPrintingBuffer_view) {
                if (num > 0x20 && num <= 0x7e) {
                    printingString += String.fromCharCode(num)
                } else if (num == 0x20) {
                    // Convert space characters into non-breaking space characters. This prevents
                    // the line wrap from occuring anywhere other than on a character boundary
                    printingString += "\u00a0"
                } else {
                    // Convert non-printing characters into a fixed-width unicode symbol
                    printingString += "\u00af"
                }
            }

            // Convert buffer to hex string
            let hexString = arrayToHex(nonPrintingBuffer_view)

            // Set `text`
            if (mustRedrawAll === true) {
                outputTextArea.text = printingString

                // Convert array to HEX string and set `text`
                outputHexArea.text = hexString
            } else {
                // Use the insert command so that we don't accidentally clobber the freshly inserted `\u00A0` characters
                outputTextArea.insert(outputTextArea.text.length, printingString)

                // Check if this is the very first byte, or if there's nothing to add
                if (outputHexArea.text.length === 0 || nonPrintingBuffer_view.length === 0) {
                    // Append without adding a space
                    // Use the insert command so that we don't accidentally scroll the view
                    outputHexArea.insert(outputHexArea.text.length, hexString)
                } else {
                    // Append while adding a space
                    // Use the insert command so that we don't accidentally scroll the view
                    outputHexArea.insert(outputHexArea.text.length, " " + hexString)
                }
            }

            generateHexRowAddresses();


        } else {
            let nonPrintingString = ""

            // Determine if we are regenerating the entire text document
            let tmpArray
            if (mustRedrawAll === true) {
                tmpArray = internal.canonicalArray
            } else {
                tmpArray = nonPrintingBuffer_view
            }

            // Iterate over all the characters in the array
            for (let num of tmpArray) {
                // Skip over the null character so it isn't confused for an end-of-string directive
                if (num > 0x00) {
                    nonPrintingString += String.fromCharCode(num)
                }
            }

            //Determine if we are regenerating the entire document
            if (mustRedrawAll === true) {
                outputTextArea.text = nonPrintingString
            } else {
                outputTextArea.text = outputTextArea.text + nonPrintingString
            }
        }

        // Determine if the view should scroll to the end or not
        if (pos === endPos) {
            scrollToEnd();
        } else {
            setPos(pos);
        }
    }

    function currPos() {
        return console_Flickable.contentY
    }

    function setPos(pos) {
        console_Flickable.contentY = pos;
    }

    function getEndPos() {
        let ratio = 1.0 - console_Flickable.visibleArea.heightRatio;
        let endPos = Math.round(console_Flickable.contentHeight * ratio);
        return endPos;
    }

    function scrollToEnd() {
        console_Flickable.contentY = getEndPos();
    }

    function concatTypedArrays(a, b) { // a, b TypedArray of same type
        var c = new (a.constructor)(a.length + b.length);
        c.set(a, 0);
        c.set(b, a.length);
        return c;
    }
}
