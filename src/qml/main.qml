import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QlChannelSerial 1.0
import FileIO 1.0

import Qt.labs.settings 1.0

ApplicationWindow {
    id: mainWindow

    visible: true

    minimumWidth: 800
    minimumHeight: 600

    title: qsTr("AeroTerm")

    signal playbackReachedEndOfLogFile();

    Shortcut {
       context: Qt.ApplicationShortcut
       sequences: ["Ctrl+D"]

        onActivated: {
            console.log("Connect/disconnect shortcut activated.")
            terminal.bn_connectPort.clicked()
        }
    }

    Component.onCompleted: {
        // Initialize combo box list
        repopulateComboBox();
    }

    function repopulateComboBox() {
        var availableChannels = serial.channels()
        terminal.comPortListModel.clear()
        for (var i=0; i<Object.keys(availableChannels).length; i++) {
            console.log("Ch: " + availableChannels[i])
            terminal.comPortListModel.append({key: availableChannels[i], value: availableChannels[i]})
        }

        // Reset the combobox to the first element
        terminal.cb_comPort.currentIndex = 0
    }

    Timer {
        id: serialPortExistenceTimer

        interval: 1000
        repeat: true
        running: false

        onTriggered: {
            if (terminal.isConnected === false) {
                // Attempt to connect to serial port
                terminal.bn_connectPort.clicked()
            } else {
                stop()
            }
        }
    }

    QlChannelSerial {
        id: serial

        onErrorOccurred: function(serialPortError){
            if (serialPortError !== 0) {
                if (serialPortError === 6) {
                    console.log("[Error][Serial port] " + serial.name() + " no longer exists")
                    terminal.bn_connectPort.clicked()

                    if (terminal.autoReconnectState === true) {
                        serialPortExistenceTimer.start()
                    }
                } else if (serialPortError === 1) {
                } else {
                    console.log("[Error][Serial port] Error value: " + serialPortError + ". See https://doc.qt.io/qt-6/qserialport.html#SerialPortError-enum for error value descriptions.")
                }
            }
        }

        onReadyRead: {
            // Returns a QByteArray which maps to an ArrayBuffer
            let rx_data = serial.readBytes();

            // Check if the buffer has any data in it
            if (rx_data.byteLength > 0) {
                // Check if we should write data to the log file
                if (fileIO.isLoggingActive) {
                    // Convert received binary data into hex-escaped string
                    let loggedData = hexadecimalEscapeArrayBuffer(rx_data)

                    // Write the data format in JSON as a hex-escaped string with an associated timestamp.
                    let output = {}
                    output.timestamp = Date.now()
                    output.rx_data = loggedData

                    fileIO.write(JSON.stringify(output) + "\n")

                    // Start the buffer flush timer
                    timer_flushOutputFile.start()
                }

                // Write the new data to the console view
                terminal.consoleView.updateText(rx_data, false)
            }

        }
    }

    // Periodically flush the buffer
    Timer {
        id: timer_flushOutputFile
        interval: 3000  // Every three seconds seems like a reasonable rate for flushing the buffer to disk

        onTriggered: {
            console.log("Flushing!")
            fileIO.flush()
        }
    }

    FileIO {
        id: fileIO
        property bool isLoggingActive: false
        property bool isPlaybackActive: false
        property bool isPathValid: false

        onFilenameChanged: {
            console.log("Filename: " + filename)
        }
    }

    Item {
        id: consoleOutputKeyHandler

        Keys.onPressed: {
            if (terminal.sw_ConsoleStyle.checked) {
                var ret = serial.writeString(event.text);

                if (ret > 0) {
                    terminal.consoleView.updateText(event.text, false)
                }
            }
            event.accepted = true;
        }
    }

    SerialTerminal {
        id: terminal

        anchors.fill: parent

        bn_rescanPorts.onClicked: {
            repopulateComboBox();
        }

        onBaudRateChanged: {
            // Set the new baud rate
            serial.paramSet('baud', baudRate);
            var actualBaudRate = serial.param('baud')

            // Return an error if the new baud rate did not set
            if (parseInt(actualBaudRate, 10) !== baudRate) {
                console.log("[WARNING] Requested baud rate: " + baudRate + ". Actual baud rate: " + actualBaudRate)
            } else {
                console.log("New baud rate: " + actualBaudRate)
            }
        }

        onParityChanged: {
            // Set the new parity
            serial.paramSet('parity', parity);
            let actualParity = serial.param('parity')

            // Return an error if the new parity did not set
            if (actualParity !== parity.charAt(0)) {
                console.log("[WARNING] Requested parity: " + parity + ". Actual parity: " + actualParity)
            } else {
                console.log("New parity: " + parity)
            }
        }

        onDataBitsChanged: {
            // Set the new number of data bits
            serial.paramSet('bits', dataBits);
            var actualDataBits = serial.param('bits')

            // Return an error if the new number of data bits did not set
            if (parseInt(actualDataBits, 10) !== dataBits) {
                console.log("[WARNING] Requested number of data bits: " + dataBits + ". Actual number of data bits: " + actualDataBits)
            } else {
                console.log("New number of data bits: " + actualDataBits)
            }
        }

        onStopBitsChanged: {
            // Set the new number of stop bits
            serial.paramSet('stops', stopBits);
            var actualStopBits = serial.param('stops')

            // Return an error if the new number of stop bits did not set
            if (parseFloat(actualStopBits) !== stopBits) {
                console.log("[WARNING] Requested number of stop bits: " + stopBits + ". Actual number of stop bits: " + actualStopBits)
            } else {
                console.log("New number of data bits: " + actualStopBits)
            }
        }


        bn_connectPort.onClicked: {
            if (terminal.isConnected !== true) {
                openSerialPort();
            } else {
                closeSerialPort();
            }
        }

        Connections {
            target: terminal
            function onSelectedPortChanged() {
                if (terminal.isConnected === true) {
                    closeSerialPort();
                    openSerialPort();
                }
            }
        }
    }

    Timer {
        repeat: true
        running: false
        interval: 1
        triggeredOnStart: true

        id: playbackTimer

        property var jsonObject

        property bool isNextMessageEmpty: true

        property var timeStampOffset: undefined
        property var wallTimeOffset: undefined

        property real timeCompression: 1


        onTriggered: {
            // Loop until all messages which fall in the time slot are read
            do {
                if (isNextMessageEmpty === true) {
                    // Read a JSON object string
                    let jsonString = fileIO.readStreamingJSON();

                    // Check if this is the end of the file
                    if (jsonString.byteLength === 0) {
                        playbackReachedEndOfLogFile();

                        return;
                    }

                    // Parse the JSON string into a JS object
                    jsonObject = JSON.parse(jsonString);

                    isNextMessageEmpty = false;

                    // Check if this is the first packet
                    if (timeStampOffset === undefined) {
                        timeStampOffset = jsonObject.timestamp
                    }
                }

                // Check if the time has gone past the JSON time
                if ((Date.now()-wallTimeOffset)*timeCompression >= (jsonObject.timestamp - timeStampOffset)) {
                    isNextMessageEmpty = true

                    // Check that there is a received data field
                    if(jsonObject.hasOwnProperty('rx_data'))
                    {
                        // Convert the text into a byte array
                        let [outputArray, idx] = hexadecimalEscapeToArr(jsonObject.rx_data)

                        // Send bytes
                        let ret = serial.writeBytes(outputArray.buffer);
                    }

                }
            } while (isNextMessageEmpty === true);
        }
    }

    Settings {
        id: lastApplicationState
        category: "mainLastApplicationState"

        property alias x: mainWindow.x
        property alias y: mainWindow.y
        property alias width: mainWindow.width
        property alias height: mainWindow.height
    }

    Settings {
        category: "TerminalSettings"
        property alias baudRate: terminal.baudRate
        property alias baudRateCustom: terminal.customBaudRateText
        property alias autoReconnectState: terminal.autoReconnectState
    }

    function closeSerialPort() {
        serial.close()
        terminal.isConnected = false;
        console.log("Closed port!")
    }

    function openSerialPort() {
        // open first available port
        serial.open(terminal.selectedPort);
        console.log("Ch: " + serial.channels())

        // if success - configure port parameters
        if (serial.isOpen()){
            terminal.isConnected = true;

            serial.paramSet('baud', terminal.baudRate);
            serial.paramSet('bits', terminal.dataBits);
            serial.paramSet('parity', terminal.parity);
            serial.paramSet('stops', terminal.stopBits);

            console.log("Successfully opened port: "  +
                        serial.name() + ", " +
                        serial.param('baud') + " baud, " +
                        serial.param('bits')  + " bits, " +
                        serial.param('parity')  + " parity, " +
                        serial.param('stops') + " stops" )
        }
    }

    /* Convert array of bytes to hexadecimal-escaped string
     */
    function hexadecimalEscapeArrayBuffer(arrayBuffer) {
        let escapedHexString    = ''

        let bytes = new Uint8Array(arrayBuffer)

        // Iterate over each byte
        for (let value of bytes) {
            // If the value is printable ASCII, then keep it. Otherwise, hex-escape it (making sure to also escape the `\`).
            escapedHexString +=
                    value >= 0x20 && value < 0x7f ? String.fromCharCode(value) : '\\x' + value.toString(16).padStart(2, '0')

        }

        return escapedHexString
    }

    // Converts the input hexadecimal-escaped string into a byte array. I.e., the
    // string `1234\x0a\x00` becomes `0x31 0x32 0x33 0x34 0x0A 0x00`.
    function hexadecimalEscapeToArr(stringHexEscape) {
        // Regex to find escaped hexadecimal. Note that it requies looking for the escaped `\`
        const regex = /\\x([\da-f]{2})|([ -~])/g;

        let tmpArray = new Uint8Array(stringHexEscape.length)
        let idx = 0
        let m

        // Perform a regex, looking for any `\x` hex escaped bytes. This seems slow
        // and inefficient, but it works and it's not a high-priority/performance loop
        while ((m = regex.exec(stringHexEscape)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }

            if (m[1] !== undefined) {
                // This is a hex value. Place it in after converting it to a number.
                tmpArray[idx] = parseInt(m[1], 16);
            } else if (m[2] !== undefined) {
                // This is an ascii value. Place it in after converting it to a number.
                tmpArray[idx] = m[2].charCodeAt(0);
            }
            idx++
        }

        // Use `slice()` and not `subarray()`, as slice returns a new Array and subarray does
        // not. If the old array is used, then the entire array will be transferred with `.buffer`,
        // even if `subarray()` has been used.
        let outputArray = tmpArray.slice(0,idx)

        return [outputArray, idx]
    }

}
