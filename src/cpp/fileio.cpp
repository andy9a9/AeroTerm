#include "fileio.h"
#include <QDebug>


FileIO::FileIO()
{

}

FileIO::~FileIO()
{
  file.close();
}

void FileIO::writeString(QString data)
{
  if(file.open(QIODevice::WriteOnly)){
    QTextStream stream(&file);
    stream << data << Qt::endl;
  }
}

qint64 FileIO::write(QByteArray data)
{
  if (data.length() == 0) {
    return 0;
  }

  qint64 ret = file.write(data);
  return ret;
}

QByteArray FileIO::readStreamingJSON()
{
   QByteArray line = file.readLine();
   return line;
}

bool FileIO::openWriteOnly()
{
  return file.open(QIODevice::WriteOnly);
}

bool FileIO::openReadOnly()
{
  return file.open(QIODevice::ReadOnly);
}

void FileIO::flush()
{
  file.flush();
}

void FileIO::close()
{
  file.close();
}

