#include "ql-channel-serial.hpp"

QlChannelSerial::QlChannelSerial(QObject* parent) :
    QlChannel(parent)
{
	port_ = new QSerialPort();
	open_ = false;
	params_ << "baud" << "bauds" << "bits" << "parity" << "stops" << "rts" << "cts" << "dtr" << "dsr" << "dcd" << "ri";

    connect(port_, SIGNAL(readyRead()), this, SIGNAL(readyRead()));
    connect(port_, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SIGNAL(errorOccurred(QSerialPort::SerialPortError)));
}

QStringList QlChannelSerial::channels() {
	QList<QSerialPortInfo> pil = QSerialPortInfo::availablePorts();
	QStringList psl = QStringList();
	for (int i=0; i<pil.size(); i++){ psl.append( pil[i].portName() ); }
	return psl;
}

bool QlChannelSerial::open(const QString &name) {
	close();
	port_->setPortName(name);
	open_ = port_->open(QIODevice::ReadWrite);
	if (open_) name_ = name; else name_ = QString::fromLatin1("");
	return open_;
}

void QlChannelSerial::close() {
	if (isOpen()){
		port_->close();
		open_ = false;
	}
}

bool QlChannelSerial::isOpen() { return open_; }

QString QlChannelSerial::name() { return name_; }

QString QlChannelSerial::param(const QString &name) {
	if (!isOpen()) return QString::fromLatin1("");
	
	if (name == "baud") return QString::number(port_->baudRate());

	else if (name == "bits") return QString::number(port_->dataBits());

	else if (name == "parity") return QString::fromLatin1(
		(port_->parity() == QSerialPort::NoParity)    ? "n" :
		(port_->parity() == QSerialPort::EvenParity)  ? "e" :
		(port_->parity() == QSerialPort::OddParity)   ? "o" :
		(port_->parity() == QSerialPort::SpaceParity) ? "s" :
		(port_->parity() == QSerialPort::MarkParity)  ? "m" : "u"
		);

	else if (name == "stops") return QString::number(port_->stopBits());

	else if (name == "rts") return QString::number(port_->isRequestToSend());
	else if (name == "cts") return QString::number(!!(port_->pinoutSignals() & QSerialPort::ClearToSendSignal));
	else if (name == "dtr") return QString::number(port_->isDataTerminalReady());
	else if (name == "dsr") return QString::number(!!(port_->pinoutSignals() & QSerialPort::DataSetReadySignal));
	else if (name == "dcd") return QString::number(!!(port_->pinoutSignals() & QSerialPort::DataCarrierDetectSignal));
	else if (name == "ri")  return QString::number(!!(port_->pinoutSignals() & QSerialPort::RingIndicatorSignal));

	else return QString::fromLatin1("");
}

bool QlChannelSerial::paramSet(const QString &name, const QString &value) {
	if (!isOpen()) return false;
	
	if (name == "baud")        return port_->setBaudRate(value.toInt());
	else if (name == "bits")   return port_->setDataBits((QSerialPort::DataBits)value.toInt());
	else if (name == "parity") return port_->setParity(
		value.startsWith("n") ? QSerialPort::NoParity :
		value.startsWith("e") ? QSerialPort::EvenParity :
		value.startsWith("o") ? QSerialPort::OddParity :
		value.startsWith("s") ? QSerialPort::SpaceParity :
		value.startsWith("m") ? QSerialPort::MarkParity :
		QSerialPort::NoParity
	);
	else if (name == "stops") return port_->setStopBits((QSerialPort::StopBits)value.toInt());
	else if (name == "rts")   return port_->setRequestToSend((bool)value.toInt());
	else if (name == "dtr")   return port_->setDataTerminalReady((bool)value.toInt());
	else return false;
}


QByteArray QlChannelSerial::readBytes() {
    // No need to check for available bytes, `readAll()`
    // returns an empty QByteArray if there is nothing
    // in the serial-in buffer
    QByteArray buf = port_->readAll();
    return buf;
}

qint64 QlChannelSerial::writeBytes(const QByteArray &l) {

    // Only write bytes if the port is open
	if (isOpen()){
        // If nothing to write, then return 0 (and not -1, because the user
        // has asked for 0 bytes to be written, and 0 bytes were written.)
        if (!l.size()) {
            return 0;
        }

        // Reallocate memory for `out_`
		out_->resize(l.size());

        // Transfer data from `l` to `out_`
        for (int i=0; i<l.size(); i++) {
            out_->data()[i] = l[i];
        }

        // Send data to serial port
		return port_->write(*out_);
	}

	return -1;
}

qint64 QlChannelSerial::writeString(const QString &s) {
	if (isOpen()){
        // Receive data from serial port
		return port_->write(s.toLatin1());
	}
	return -1;
}

