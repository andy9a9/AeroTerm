#ifndef QL_CHANNEL_SERIAL_H

#define QL_CHANNEL_SERIAL_H

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QIODevice>
#include <QString>
#include <QDebug>
#include "ql-channel.hpp"

class QlChannelSerial : public QlChannel {
    Q_OBJECT

    public:
        QlChannelSerial(QObject* parent = 0);

        Q_INVOKABLE virtual QStringList channels();

        Q_INVOKABLE virtual bool open(const QString &name);
        Q_INVOKABLE virtual void close();
        Q_INVOKABLE virtual bool isOpen();
        Q_INVOKABLE virtual QString name();

        Q_INVOKABLE virtual QString param(const QString &name);
        Q_INVOKABLE virtual bool paramSet(const QString &name, const QString &value);

        Q_INVOKABLE virtual QByteArray readBytes();
        Q_INVOKABLE virtual qint64 writeBytes(const QByteArray &b);

        Q_INVOKABLE virtual qint64 writeString(const QString &s);

    protected:
        QSerialPort *port_;
        bool open_;
};

#endif  /* QL_CHANNEL_SERIAL_H */

