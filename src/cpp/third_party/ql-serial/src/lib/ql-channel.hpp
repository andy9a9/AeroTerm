#ifndef QL_CHANNEL_H

#define QL_CHANNEL_H

#include <QObject>
#include <QString>
#include <QStringList>
#include <QList>
#include <QVariant>

#include <QSerialPort>

class QlChannel : public QObject {
    Q_OBJECT

    public:
        QlChannel(QObject* parent = 0);

        Q_INVOKABLE virtual QStringList channels() = 0;

        Q_INVOKABLE virtual bool open(const QString &name) = 0;
        Q_INVOKABLE virtual void close() = 0;
        Q_INVOKABLE virtual bool isOpen() = 0;
        Q_INVOKABLE virtual QString name() = 0;

        Q_INVOKABLE virtual QStringList params();
        Q_INVOKABLE virtual QString param(const QString &name) = 0;
        Q_INVOKABLE virtual bool paramSet(const QString &name, const QString &value) = 0;

        Q_INVOKABLE virtual QByteArray readBytes() = 0;
        Q_INVOKABLE virtual qint64 writeBytes(const QByteArray &b) = 0;
        Q_INVOKABLE virtual qint64 writeString(const QString &s) = 0;

    protected:
        QString     name_;
        QStringList params_;
        QByteArray  *out_;
	
    private:

    signals:
        void readyRead();
        void errorOccurred(QSerialPort::SerialPortError serialPortError);
};

#endif  /* QL_CHANNEL_H */

