#include "ql-channel.hpp"

QlChannel::QlChannel(QObject* parent) :
    QObject(parent)
{
    name_ = QString("");
    params_ = QStringList();
    out_ = new QByteArray();
}


QStringList QlChannel::params() { return params_; }


