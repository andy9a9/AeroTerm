#ifndef FILEIO_H
#define FILEIO_H

#include <QFile>
#include <QObject>
#include <QUrl>

class FileIO : public QObject {
    Q_OBJECT

    Q_PROPERTY(QUrl filename READ filename WRITE setFilename NOTIFY filenameChanged)

public:
    FileIO();
    Q_INVOKABLE void writeString(QString data);
    Q_INVOKABLE qint64 write(QByteArray data);
    Q_INVOKABLE QByteArray readStreamingJSON();
    Q_INVOKABLE bool openWriteOnly();
    Q_INVOKABLE bool openReadOnly();
    Q_INVOKABLE void close();
    Q_INVOKABLE void flush();

    ~FileIO();

public:
    void setFilename(const QUrl &a) {
        if (a.toLocalFile() != file.fileName()) {
            file.setFileName(a.toLocalFile());
            emit filenameChanged();
        }
    }

    QUrl filename() const {
        return QUrl(file.fileName());
    }

signals:
    void filenameChanged();

private:
    QFile file;
};

#endif  /* FILEIO_H */
