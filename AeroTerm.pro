QT += \
    qml \
    quick \
    serialport \
    quickcontrols2 \

CONFIG += c++17

SOURCES += \
    src/main.cpp \
    src/cpp/fileio.cpp \
    src/cpp/third_party/ql-serial/src/lib/ql-channel-serial.cpp \
    src/cpp/third_party/ql-serial/src/lib/ql-channel.cpp \
    src/cpp/third_party/QtSyntaxHighlighter/SyntaxHighlighter.cpp \
    src/cpp/third_party/QtSyntaxHighlighter/TextCharFormat.cpp \


HEADERS += \
    src/cpp/fileio.h \
    src/cpp/third_party/ql-serial/src/lib/ql-channel-serial.hpp \
    src/cpp/third_party/ql-serial/src/lib/ql-channel.hpp \
    src/cpp/third_party/QtSyntaxHighlighter/SyntaxHighlighter.h \
    src/cpp/third_party/QtSyntaxHighlighter/TextCharFormat.h \

RESOURCES += \
    src/qml/qml.qrc \
    Resources/Resources.qrc \

DISTFILES += \
    src/qml/*.qml \

OTHER_FILES += \
    .gitlab-ci.yml \
    README.md \

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# Set icon in macOS
macx {
   ICON = Resources/app_icon/AeroTerm.icns
}

# Set icon in Windows
win32 {
   RC_ICONS = Resources/app_icon/AeroTerm.ico
}
